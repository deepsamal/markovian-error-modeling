% Load Results from all experiments
% Show trajectory of drones

actual_out = load("Results\car6-actual-out.mat");
recall_out = load("Results\car6-recall-out.mat");
hmm_out = load("Results\car6-HMM_4-out.mat");

target_traj = squeeze(actual_out.out.targetTrajectoryPoints(1,:,:))';
actual_traj = squeeze(actual_out.out.trajectoryPoints(1,:,:))';
recall_traj = squeeze(recall_out.out.trajectoryPoints(1,:,:))';
hmm_traj = squeeze(hmm_out.out.trajectoryPoints(1,:,:))';

% Plot the trajectories in 3D
plot3(target_traj(:,2),target_traj(:,1),-target_traj(:,3),"-k", 'linewidth',3);
hold on;
plot3(actual_traj(:,2),actual_traj(:,1),-actual_traj(:,3),"-b", 'linewidth',3);
hold on;
plot3(recall_traj(:,2),recall_traj(:,1),-recall_traj(:,3),"-r", 'linewidth',3);
hold on;
% plot3(hmm_traj(:,2),hmm_traj(:,1),-hmm_traj(:,3),"-g", 'linewidth',3);
% lgd = legend(["Target", "Real", "Recall", "HMM"]);
lgd = legend(["Target", "Real", "Recall"]);
lgd.Location = 'north';
lgd.FontSize = 15;

xlabel('East');
ylabel('North');
zlabel('Down');