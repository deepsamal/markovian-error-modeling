% openExample('uav/UAVObstacleAvoidanceInSimulinkExample')

% Create Scenario
Scenario = uavScenario("UpdateRate",100,"ReferenceLocation",[0 0 0]);
addMesh(Scenario,"cylinder",{[0 0 1] [0 .01]},[0 1 0]);

InitialPosition = [0 0 0];
InitialOrientation = [0 0 0];

TargetInitialPosition = [0 15 0];

% Define UAV Platform
platUAV = uavPlatform("UAV",Scenario, ...
                      "ReferenceFrame","NED", ...
                      "InitialPosition",InitialPosition, ...
                      "InitialOrientation",eul2quat(InitialOrientation));

updateMesh(platUAV,"quadrotor",{1.2},[0 0 1],eul2tform([0 0 pi]));

platTargetUAV = uavPlatform("TargetUAV",Scenario, ...
                      "ReferenceFrame","NED", ...
                      "InitialPosition",InitialPosition, ...
                      "InitialOrientation",eul2quat(InitialOrientation));

updateMesh(platTargetUAV,"quadrotor",{1.2},[0 0 1],eul2tform([0 0 pi]));

% Create and Mount Sensor Model
AzimuthResolution = 0.5;      
ElevationResolution = 2;

MaxRange = 7;
AzimuthLimits = [-179 179];
ElevationLimits = [-15 15];

LidarModel = uavLidarPointCloudGenerator("UpdateRate",10, ...
                                         "MaxRange",MaxRange, ...
                                         "RangeAccuracy",3, ...
                                         "AzimuthResolution",AzimuthResolution, ...
                                         "ElevationResolution",ElevationResolution, ...
                                         "AzimuthLimits",AzimuthLimits, ...
                                         "ElevationLimits",ElevationLimits, ...                                       
                                         "HasOrganizedOutput",true);

uavSensor("Lidar",platUAV,LidarModel, ...
          "MountingLocation",[0 0 -0.4], ...
          "MountingAngles",[0 0 180]);

uavSensor("Lidar",platTargetUAV,LidarModel, ...
          "MountingLocation",[0 0 -0.4], ...
          "MountingAngles",[0 0 180]);

show3D(Scenario);

% Add Obstacles to Scenario
% ObstaclePositions = [10 0; 20 10; 10 20]; % Locations of the obstacles
% ObstacleHeight = 15;                      % Height of the obstacles
% ObstaclesWidth = 3;                       % Width of the obstacles
% 
% for i = 1:size(ObstaclePositions,1)
%     addMesh(Scenario,"polygon", ...
%         {[ObstaclePositions(i,1)-ObstaclesWidth/2 ObstaclePositions(i,2)-ObstaclesWidth/2; ...
%         ObstaclePositions(i,1)+ObstaclesWidth/2 ObstaclePositions(i,2)-ObstaclesWidth/2; ...
%         ObstaclePositions(i,1)+ObstaclesWidth/2 ObstaclePositions(i,2)+ObstaclesWidth/2; ...
%         ObstaclePositions(i,1)-ObstaclesWidth/2 ObstaclePositions(i,2)+ObstaclesWidth/2], ...
%         [0 ObstacleHeight]},0.651*ones(1,3));
% end
% show3D(Scenario);
% legend("Start Position","Obstacles")

% Model Overview
open_system("DroneFollower.slx");

% Specify the waypoints for the UAV.
% Waypoints = [TargetInitialPosition; 100 0 -7; 100 100 -7; 0 100 -7];
Waypoints = [TargetInitialPosition; 40 40 0; 100 100 0];
for i = 2:size(Waypoints,1)
    addMesh(Scenario,"cylinder",{[Waypoints(i,2) Waypoints(i,1) 1] [0 0.1]},[1 0 0]);
end
show3D(Scenario);
hold on
plot3([TargetInitialPosition(1,2); InitialPosition(1,2); Waypoints(:,2)],[TargetInitialPosition(1,2); InitialPosition(1,2); Waypoints(:,1)],[-TargetInitialPosition(1,3); -InitialPosition(1,3); -Waypoints(:,3)],"-g")
legend(["Start Position","Obstacles","","","Waypoints","","","Direct Path"])

% Controller and plant
% Target Proportional Gains
Px_t = 6;
Py_t = 6;
Pz_t = 6.5;

% Target Derivative Gains
Dx_t = 1.5;
Dy_t = 1.5;
Dz_t = 2.5;

% Target Integral Gains
Ix_t = 0;
Iy_t = 0;
Iz_t = 0;

% Chasing Proportional Gains
Px_c = 1;
Py_c = 1;
Pz_c = 1.5;

% Derivative Gains
Dx_c = 1.5;
Dy_c = 1.5;
Dz_c = 2.5;

% Integral Gains
Ix_c = 0;
Iy_c = 0;
Iz_c = 0;

% Filter Coefficients
Nx = 10;
Ny = 10;
Nz = 14.4947065605712; 

UAVSampleTime = 0.001;
Gravity = 9.81;
DroneMass = 0.1;

% Load Detections
% Real detections
% detections = readmatrix('../ErrorModels/ssd_mobilenet_v2_320x320_coco17_tpu-8/car5.csv');
% detections = readmatrix('../ErrorModels/ssd_mobilenet_v2_320x320_coco17_tpu-8/HMM_4/random.csv');
% detections = readmatrix('../ErrorModels/ssd_mobilenet_v2_320x320_coco17_tpu-8/HMM_4/car5_predictions.csv');
% % detections = readmatrix('../ErrorModels/ssd_mobilenet_v2_320x320_coco17_tpu-8/NAR/car5_predictions.csv');
sigmas = [0.0001 0.001 0.01 0.1];
% detections = readmatrix('../ErrorModels/ssd_mobilenet_v2_320x320_coco17_tpu-8/HMM_4/modified/0.1/car5_predictions.csv');
out = simulate(detections);
disp(out.positionError(end))

function out = simulate(detections)
    % Simulate Model
    out = sim("DroneFollower.slx");
    
    % Visualize Obstacle-Free UAV trajectory
    % hold on
    figure;
    points = squeeze(out.trajectoryPoints(1,:,:))';
    plot3(points(:,2),points(:,1),-points(:,3),"-r");
    % hold on
    figure;
    targetPoints = squeeze(out.targetTrajectoryPoints(1,:,:))';
    plot3(targetPoints(:,2),targetPoints(:,1),-targetPoints(:,3),"-b");
    % legend(["Start Position","Obstacles","","","Waypoints","","","Direct Path","UAV Trajectory", "Target UAV Trajectory"])
end