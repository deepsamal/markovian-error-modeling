% Create a UAV scenario and set its local origin.
Scenario = uavScenario("UpdateRate",100,"ReferenceLocation",[0 0 0]);

% Add a marker to indicate the start pose of the UAV.
addMesh(Scenario,"cylinder",{[0 0 1] [0 .01]},[0 1 0]);

% Specify the initial position and orientation of the UAV in the north-east-down (NED) frame.
InitialPosition = [0 0 -7];
InitialOrientation = [0 0 0];

% Create a UAV platform in the scenario.
platUAV = uavPlatform("UAV",Scenario, ...
                      "ReferenceFrame","NED", ...
                      "InitialPosition",InitialPosition, ...
                      "InitialOrientation",eul2quat(InitialOrientation));

% Add a quadrotor mesh for visualization.
updateMesh(platUAV,"quadrotor",{1.2},[0 0 1],eul2tform([0 0 pi]));

% Specify the lidar resolution.
AzimuthResolution = 0.5;      
ElevationResolution = 2;

% Specify the lidar range.
MaxRange = 7;
AzimuthLimits = [-179 179];
ElevationLimits = [-15 15];

% Create a statistical sensor model to generate point clouds for the lidar sensor.
LidarModel = uavLidarPointCloudGenerator("UpdateRate",10, ...
                                         "MaxRange",MaxRange, ...
                                         "RangeAccuracy",3, ...
                                         "AzimuthResolution",AzimuthResolution, ...
                                         "ElevationResolution",ElevationResolution, ...
                                         "AzimuthLimits",AzimuthLimits, ...
                                         "ElevationLimits",ElevationLimits, ...                                       
                                         "HasOrganizedOutput",true);

% Create a lidar sensor and mount the sensor on the quadrotor.
uavSensor("Lidar",platUAV,LidarModel, ...
          "MountingLocation",[0 0 -0.4], ...
          "MountingAngles",[0 0 180]);

% Preview the scenario using the show3D function.
show3D(Scenario);

% Specify the waypoints for the UAV.
Waypoints = [InitialPosition; 0 20 -7; 20 20 -7; 20 0 -7];

% Add markers to indicate the waypoints.
for i = 2:size(Waypoints,1)
    addMesh(Scenario,"cylinder",{[Waypoints(i,2) Waypoints(i,1) 1] [0 0.1]},[1 0 0]);
end
show3D(Scenario);
hold on
plot3([InitialPosition(1,2); Waypoints(:,2)],[InitialPosition(1,2); Waypoints(:,1)],[-InitialPosition(1,3); -Waypoints(:,3)],"-g")
legend(["Start Position","Obstacles","","","Waypoints","","","Direct Path"])

% Specify the controller parameters. These parameters are based on a hit-and-trial approach, and can be tuned for a smoother flight.
% Proportional Gains
Px = 6;
Py = 6;
Pz = 6.5;

% Derivative Gains
Dx = 1.5;
Dy = 1.5;
Dz = 2.5;

% Integral Gains
Ix = 0;
Iy = 0;
Iz = 0;

% Filter Coefficients
Nx = 10;
Ny = 10;
Nz = 14.4947065605712; 

% Specify gravity, drone mass, and sample time for the controller and plant blocks.
UAVSampleTime = 0.001;
Gravity = 9.81;
DroneMass = 0.1;

% Model Overview
open_system("ObstacleSimulation.slx");

% Load detections
% detections = readmatrix('../ErrorModels/ssd_mobilenet_v2_320x320_coco17_tpu-8/car5.csv');
% detections = readmatrix('../ErrorModels/ssd_mobilenet_v2_320x320_coco17_tpu-8/HMM_4/random.csv');
detections = readmatrix('../ErrorModels/ssd_mobilenet_v2_320x320_coco17_tpu-8/HMM_4/car5_predictions.csv');
% detections = readmatrix('../ErrorModels/ssd_mobilenet_v2_320x320_coco17_tpu-8/HMM_4/modified/0.1/car5_predictions.csv');

% Add cuboid obstacles, representing buildings, to the scenario.
ObstaclePositions = [10 0; 20 10; 10 20]; % Locations of the obstacles
ObstacleHeight = 15;                      % Height of the obstacles
ObstaclesWidth = 3;                       % Width of the obstacles

for i = 1:size(ObstaclePositions,1)
    addMesh(Scenario,"polygon", ...
        {[ObstaclePositions(i,1)-ObstaclesWidth/2 ObstaclePositions(i,2)-ObstaclesWidth/2; ...
        ObstaclePositions(i,1)+ObstaclesWidth/2 ObstaclePositions(i,2)-ObstaclesWidth/2; ...
        ObstaclePositions(i,1)+ObstaclesWidth/2 ObstaclePositions(i,2)+ObstaclesWidth/2; ...
        ObstaclePositions(i,1)-ObstaclesWidth/2 ObstaclePositions(i,2)+ObstaclesWidth/2], ...
        [0 ObstacleHeight]},0.651*ones(1,3));
end

totalSims = 1;
collisonResults = zeros(1, totalSims);

lookahead = 5;

for simNum = 1:totalSims
    lookahead = 5 + simNum;
%     ObstaclePositions(2) = ObstaclePositions(2) + simNum;
%     ObstaclesWidth = OriginalObstaclesWidth/simNum;
%     
%     for i = 1:size(ObstaclePositions,1)
%         addMesh(Scenario,"polygon", ...
%             {[ObstaclePositions(i,1)-ObstaclesWidth/2 ObstaclePositions(i,2)-ObstaclesWidth/2; ...
%             ObstaclePositions(i,1)+ObstaclesWidth/2 ObstaclePositions(i,2)-ObstaclesWidth/2; ...
%             ObstaclePositions(i,1)+ObstaclesWidth/2 ObstaclePositions(i,2)+ObstaclesWidth/2; ...
%             ObstaclePositions(i,1)-ObstaclesWidth/2 ObstaclePositions(i,2)+ObstaclesWidth/2], ...
%             [0 ObstacleHeight]},0.651*ones(1,3));
%     end
%     show3D(Scenario);
%     legend("Start Position","Obstacles")
    
    % Simulate Model
    out = sim("ObstacleSimulation.slx", 'TimeOut',60);
    
    % Visualize Obstacle-Free UAV trajectory
    hold on
    points = squeeze(out.trajectoryPoints(1,:,:))';
    plot3(points(:,2),points(:,1),-points(:,3),"-r");
    legend(["Start Position","Obstacles","","","Waypoints","","","Direct Path","UAV Trajectory"])
    
    [collison, obstacleProximity] = check_collison(points, ObstaclePositions, ObstaclesWidth);
    collisonResults(simNum) = collison;

    restart(Scenario);
end

collisonProbrability = mean(collisonResults);

function [collison, obstacleProximity] = check_collison(trajectory, obstaclePositions, obstacleWidth)
    collison = 0;
    obstacleProximity = zeros(1, size(trajectory, 1));

    for i = 1:size(trajectory, 1)
        min_distance = 1/0;
        for j = 1:size(obstaclePositions, 1)
%             obstaclePosition = obstaclePositions(j);
            distance = norm(trajectory(i, 1:2) - obstaclePositions(j, 1:2));
%             disp(distance);
            if distance < obstacleWidth/2
                collison = 1;
                return
            end
            if distance < min_distance
                min_distance = distance;
            end
        end
        obstacleProximity(i) = min_distance;
    end
end

