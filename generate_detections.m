HMM_directory = '../ErrorModels/ssd_mobilenet_v2_320x320_coco17_tpu-8/HMM_4';
txMatrix = readmatrix(fullfile(HMM_directory,'TransmissionMatrix.csv'));
emMatrix = readmatrix(fullfile(HMM_directory,'EmissionMatrix.csv'));

seqLen = 10000;
maxConsecutiveFails = 20;
recall = evaluateHMM(txMatrix, emMatrix, seqLen);
seqProbList = consFailProb(txMatrix, emMatrix, maxConsecutiveFails);
% Plot the log probabilities
% plot(seqProbList);
% hold on;

% sigmaArray = [0.1 0.01 0.001 0.0001 0.00001];
% sigmaArray = [0.01 0.007 0.005 0.003 0.001];
% sigmaArray = [0.03 0.05 0.07 0.1];
sigmaArray = [0.0001 0.001 0.01 0.1];
% sigmaArray = [0.5];
for i = 1:numel(sigmaArray)
    sigma = sigmaArray(i);
    newEmMatrix = modifyHMM(recall, txMatrix, emMatrix, sigma);
    newRecall = evaluateHMM(txMatrix, newEmMatrix, seqLen);
    disp(newRecall)
    newseqProbList = consFailProb(txMatrix, newEmMatrix, maxConsecutiveFails);
    
    % Plot the log probabilities
    plot(newseqProbList);
    hold on;

    % Save Matrix
    formatspec = 'EmissionMatrix_%0.5e.csv';
    filename = sprintf(formatspec, sigma);
    writematrix(newEmMatrix,fullfile(HMM_directory, 'modified', filename));
end
% plotNames = {'Original HMM', 'Modified HMM'};
% legend(plotNames);


function recall = evaluateHMM(txMatrix, emMatrix, seqLen)
    [seq,states] = hmmgenerate(seqLen,txMatrix,emMatrix,'Symbols',[0 1]);
    recall = sum(seq)/seqLen;
end


function newEmissionMatrix = modifyHMM(recall, txMatrix, emMatrix, sigma)
    % Create underdetermined system of equations.
    % p(0) = e00*p(s0) + e10*p(s1) + e20*p(s2) + e30*p(s3) = 1 - R
    % p(1) = e01*p(s0) + e11*p(s1) + e21*p(s2) + e31*p(s3) = R
    % e00 + e01 = 1
    % e10 + e11 = 1
    % e20 + e21 = 1
    % e30 + e31 = 1
    % 0 <= e <= 1 (Constraints)
    % p(si) = sum(tx, 2)(i) = pi
    % x = [e00, e01, e10, e11, e20, e21, e30, e31]
    % C = [p0, 0, p1, 0, p2, 0, p3, 0;
    %      0, p0, 0,  p1, 0, p2, 0, p3;
    %      1, 1,  0,  0,  0, 0,  0, 0;
    %      0, 0,  1,  1,  0, 0,  0, 0;
    %      0, 0,  0,  0,  1, 1,  0, 0;
    %      0, 0,  0,  0,  0, 0,  1, 1;]
    % d = [1-R; R; 1; 1; 1; 1]
    
    numStates = size(txMatrix, 1);
    numOutput = 2;
    p = sum(txMatrix, 1);
    C = [p(1) 0 p(2) 0 p(3) 0 p(4) 0;
           0 p(1) 0 p(2) 0 p(3) 0 p(4);
           1, 1,  0,  0,  0, 0,  0, 0;
           0, 0,  1,  1,  0, 0,  0, 0;
           0, 0,  0,  0,  1, 1,  0, 0;
           0, 0,  0,  0,  0, 0,  1, 1;];
    d = [1-recall; recall; 1; 1; 1; 1];
    % lb = zeros(numel(emMatrix), 1);
    % ub = ones(numel(emMatrix), 1);
    % reshape(emMatrix.',1,[]);
    lb = max(emMatrix - sigma, 0);
    ub = min(emMatrix + sigma, 1);
    newEmMatrix = lsqlin(C,d,[],[],[],[],lb,ub);
    newEmMatrix = reshape(newEmMatrix, [numStates numOutput]);
    % x = lsqnonneg(C, d);
    newEmissionMatrix = newEmMatrix;
end

function seqProbList = consFailProb(txMatrix, emMatrix, maxConsecutiveFails)
    % Get probability of consecutive failures from new and old Emission
    % Matrices
%     maxConsecutiveFails = 20;
    seqProbList = zeros(1,maxConsecutiveFails);
%     newseqProbList = zeros(1,maxConsecutiveFails);
    for consecutiveFails = 1:maxConsecutiveFails
        seq = zeros(1, consecutiveFails);
    %     seq(1) = 1; seq(end) = 1;
        [PSTATES,logpseq] = hmmdecode(seq,txMatrix,emMatrix,'Symbols',[0 1]);
%         [newPSTATES,newLogpseq] = hmmdecode(seq,txMatrix,newEmMatrix,'Symbols',[0 1]);
        seqProbList(consecutiveFails) = exp(logpseq);
%         newseqProbList(consecutiveFails) = exp(newLogpseq);
    end
end