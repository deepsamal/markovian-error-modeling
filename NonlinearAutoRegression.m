Sequence_directory = '../ErrorModels/ssd_mobilenet_v2_320x320_coco17_tpu-8';

% Training
% sequences = readmatrix('../ErrorModels/sequences.txt', 'FileType', 'text');
% fileID = fopen('../ErrorModels/sequences.txt','r');
% A = fscanf(fileID,'%s\n');
fid = fopen('../ErrorModels/sequences.txt');
num_sequences = 0;
sequence_detections = zeros(1);
while ~feof(fid)
    tline = fgetl(fid);
    disp(tline)
    formatspec = '%s.csv';
    filename = sprintf(formatspec, tline);
    num_sequences = num_sequences + 1;
    sequence_detections = [sequence_detections; readmatrix(fullfile(Sequence_directory, filename))];
%     if num_sequences > 20
%         break;
%     end
end
fclose(fid);


training_table = createTrainingMatrix(sequence_detections);
Mdl = fitcnet(training_table,"y_t");
trainAccuracy = 1 - loss(Mdl,training_table,"y_t", ...
    "LossFun","classiferror");

% % Testing
test_sequence_detections = readmatrix(fullfile(Sequence_directory, 'car5.csv'));
% 
% testing_table = createTrainingMatrix(test_sequence_detections);
% predictions = predict(Mdl,testing_table);
% testAccuracy = 1 - loss(Mdl,testing_table,"y_t", ...
%     "LossFun","classiferror");

predictions = ModelPrediction(test_sequence_detections, Mdl);
plot(predictions-test_sequence_detections(1:743))

% Store Detections in a csv file
% formatspec = 'car5_predictions.csv';
% filename = sprintf(formatspec, sigma);
filename = 'car5_predictions.csv';
writematrix(predictions,fullfile(Sequence_directory, 'NAR' , filename));

function training_table = createTrainingMatrix(detections)
    % Create a 2 column table (y_t-2, y_t-1, y_t)
    training_matrix = zeros(size(detections, 1) - 2, 3);
    for i=1:size(training_matrix, 1)
        % disp(detections(i));
        training_matrix(i, 1) = detections(i);
        training_matrix(i, 2) = detections(i+1);
        training_matrix(i, 3) = detections(i+2);
    end
    training_table = table(training_matrix(:, 1),training_matrix(:, 2),training_matrix(:, 3), 'VariableNames',["y_t-2", "y_t-1", "y_t"]);
end

function predictions = ModelPrediction(detections, Mdl)
    % Create a 2 column table (y_t-2, y_t-1, y_t)
    predictions = zeros(size(detections, 1) - 2);
    noise = wgn(size(predictions, 1),1,-10);
    y_t_2 = detections(1);
    y_t_1 = detections(2);
    for i=1:size(predictions, 1)
        % disp(detections(i));
%         myMatrix = zeros(1, 3);
%         myMatrix(1, 1) = y_t_2;
%         myMatrix(1, 2) = y_t_1;
        test_sample = table(y_t_2, y_t_1, 'VariableNames',["y_t-2", "y_t-1"]);
        disp(test_sample);
        y_t_2 = y_t_1;
        output = predict(Mdl,test_sample);
        output = output + noise(i);
        if output > 0.5
            output = 1;
        else
            output = 0;
        end
        predictions(i) = output;
        y_t_1 = predictions(i);
        disp(y_t_1)
    end
end